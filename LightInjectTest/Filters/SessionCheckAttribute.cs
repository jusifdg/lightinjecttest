﻿namespace LightInjectTest.Filters
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using LightInject;
    using LightInjectTest.Abstraction.Services;

    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class SessionCheckAttribute : ActionFilterAttribute
    {
        [Inject]
        public IAuthenticationService AuthenticationService { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Check if the method skips the Session Check
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(SkipSessionCheckAttribute), false).Any())
            {
                base.OnActionExecuting(filterContext);

                return;
            }

            var session = filterContext.HttpContext.Session;

            // Check if the session is a new session
            if (session != null && session.IsNewSession)
            {
                var username = "seesharper";

                // Try and look up the user, otherwise create a new user
                var user = this.AuthenticationService.GetUser(username)
                    ?? this.AuthenticationService.SetupNewUser(username);

                // Store the user in session
                if (user != null)
                {
                    session["UserModel"] = user;
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}