﻿namespace LightInjectTest.Filters
{
    using System;

    /// <summary>
    /// Attribute used to skip the global Session Check. Common for error pages to prevent an infinite loop if the session check fails
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class SkipSessionCheckAttribute : Attribute
    {
    }
}