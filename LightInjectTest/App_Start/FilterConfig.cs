﻿namespace LightInjectTest
{
    using System.Web.Mvc;
    using LightInject;
    using LightInjectTest.Filters;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters, IServiceContainer container)
        {
            // Setup filters that need dependency injection via property injection
            var sessionCheckAttribute = new SessionCheckAttribute();
            container.InjectProperties(sessionCheckAttribute);

            // Add global filters
            filters.Add(sessionCheckAttribute);
        }
    }
}
