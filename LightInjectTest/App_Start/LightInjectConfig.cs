﻿namespace LightInjectTest
{
    using LightInject;
    using LightInjectTest.Abstraction.Services;
    using LightInjectTest.Services;

    public class LightInjectConfig
    {
        public static IServiceContainer Configure()
        {
            var container = new ServiceContainer();

            // Register MVC and Web API controllers
            container.RegisterControllers();

            // Register custom services
            RegisterServices(container);

            // Enable MVC - which calls EnablePerWebRequestScope()
            container.EnableMvc();

            // Enforce annotated property injection
            container.EnableAnnotatedPropertyInjection();

            return container;
        }

        /// <summary>
        /// Registers the custom services.
        /// </summary>
        /// <param name="container">The service container.</param>
        private static void RegisterServices(ServiceContainer container)
        {
            // Register Services
            container.Register<IAuthenticationService, AuthenticationService>();
        }
    }
}