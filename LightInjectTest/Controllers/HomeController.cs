﻿namespace LightInjectTest.Controllers
{
    using System.Web.Mvc;
    using LightInject;
    using LightInjectTest.Abstraction.Services;

    public class HomeController : Controller
    {
        [Inject]
        public IAuthenticationService AuthenticationService { get; set; }

        public ActionResult Index()
        {
            // TODO - Note: property injection working.
            var userModel = this.AuthenticationService.GetUser("seesharper");

            return View();
        }
    }
}