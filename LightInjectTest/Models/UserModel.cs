﻿namespace LightInjectTest.Models
{
    using System;
    using System.Collections.Generic;

    public class UserModel
    {
        public Guid UserGuid { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public List<string> Roles { get; set; }
    }
}