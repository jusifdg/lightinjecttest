﻿namespace LightInjectTest.Services
{
    using LightInjectTest.Abstraction.Services;
    using LightInjectTest.Models;

    public class AuthenticationService : IAuthenticationService
    {
        public UserModel SetupNewUser(string username)
        {
            return new UserModel()
            {
                FirstName = username
            };
        }

        public UserModel GetUser(string username)
        {
            return new UserModel()
            {
                FirstName = username
            };
        }
    }
}