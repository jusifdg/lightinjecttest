﻿namespace LightInjectTest.Abstraction.Services
{
    using System.Security.Principal;
    using LightInjectTest.Models;

    public interface IAuthenticationService
    {
        UserModel SetupNewUser(string username);

        UserModel GetUser(string username);
    }
}
