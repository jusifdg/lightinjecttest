﻿namespace LightInjectTest
{
    using System.Web.Mvc;
    using System.Web.Routing;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            // Configure dependency injection
            var container = LightInjectConfig.Configure();

            // Register global filters providing the inversion of control container
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters, container);
        }
    }
}
